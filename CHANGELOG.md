# Changelog 

## V0.3.0 - Stringae update

### Features

- Implementation of an algorithm to reverse a text.
- Implementation of an algorithm to check if a text is a palindrome.
 
## V0.2.0 - Physicae update 

### Features 

- Implementation of a simple converter to convert distance, liter or weight unit into another one.
- Implementation of the Ohm's law.

## V0.1.0 - Mathematicae update

### Features

- Initial configuration project.
- Add the following *mathematica* algorithms :
  - Pythagorean Theorem and his converse.
  - Intercept Theorem  also known as Thales's Theorem and his converse.
  - Euclidean algorithm to compute the Greatest Common Divisor between 2 numbers.
  - LCM algorithm to compute the Least Common Multiple between 2 numbers.
